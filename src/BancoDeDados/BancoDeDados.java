/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author aluno
 */
public class BancoDeDados {
    Insercao ins = new Insercao();
    Listagem lis = new Listagem();
    public void ListAgendamentoCliente(JList listar){
    /*Mostra ao Cliente quais horarios estão disponiveis para marcar uma consulta
        EXEMPLO : SEGUNDA FEIRA DE 10 AS 14
    */ 
        lis.ListarAgendamentoCliente(listar);
    }
     public void ListDeclaraçãoFuncionario(JList listar){
       /* Mostra ao funcinario o horario que o Cliente compareceu ( e o seu nome tambem)  
       */
         lis.ListarDeclaração_Funcioario(listar);
    }
     public void ListInformaçõesGerais(JList listar){
         /*
                model.addElement("Nome registrado: "+ nome);
                model.addElement("CPF registrado: "+ Cpf);
                model.addElement("Email registrado: "+ email);
                model.addElement("telefone registrado: "+ telefone);
                model.addElement("Voçê não compareceu: "+ compareceu +" vez(es)( Ao não comparecer 3 vezes , tomaram penalidades" ); 
         */
         lis.ListarInformaçõesGerais(listar);
     }
     public int listarCodigoServiço(){
         int b = lis.listarCodigoServiço();
         return b;
     }         
     public void ListSalario_Administrador(JList listar){
         /*
         Salario de todos os funcionarios
         */
         lis.ListarSalario(listar);
     }
     public void ListSalario_Funcionario(JList listar){
         /*
         Salario do funcionario em especifico
         */
         lis.ListarSalarioFuncionario(listar);
     }
     public void ListVisualizar_Adminstrador(JList listar){
         /*
         Adminstrador ve toda os horarios dos funcionarios e dos Clientes
         */
         lis.ListarVisualizar_Adminstrador(listar);
     }
     public void ListVisualizar_Funcionario(JList listar){
         /*
        Funcionario ve toda os horarios que ele tem que trabalhar
         */
         lis.ListarVisualizar_Funcionario(listar);
     }
     public void ListVisualizar_Serviço(JList listar){
      /*
         Aparecer em um JList todos os possiveis serviços que o Funcionario pode trabalhar
         */   
         lis.ListarServiços(listar);
         
     }
     public void ListAgendamentoClienteFiltro(JList listar,int hora,int mes,int dia,String serv){
         /*
         Nessa caso occore a filtragem de horarios caso o cliente queira que tal aconteçã 
         */
         lis.ListarAgendaMudanca(listar, dia, hora, mes, serv);
     }
     public int ListCodigoFuncionario(String nome){
         return lis.listarCodigoServiçoFuncionario(nome);
     }
     public int AcessoMenuInicial(String Email ,String senha){
         /*
         Recebe um Email e uma Senha e vai fazer essa verificação dentro do codigo para ver se existe uma senha e email que batem
         caso exista vai retornar um int , se conter 0 não existe nenhum , se retornar 1 existe um cliente , se retorna 2 existe um funcionario
         se retornar 3 existe um admin
         na segunda casa ira retorna o codigo de especificação do usuario
         */
         
         int a= lis.LoginInicial(Email, senha);
         return a;
     }
            
      public void InserirMudancaAgendamentoCliente(String day,String horario,String mes,String ano,String Funcionario,String Codigo){
      /* horario = horario do Cliente
         day = integer de 1 a 7 com os dias da semana     
         mes = integer 1  a 12
         ano = integer de 2019 a 2999
         Funcionario = String Funcionario
         Codigo = String Codigo
      */     
        ins.InserirAgendamentoCliente(day,horario,mes,ano,Funcionario,Codigo);
        }
    public void InserirCadastroHorarioFuncionario(String dia,String HorarioCome,String HorarioFinal,String mes,String ano,String funcionario){
        /*DiaSem = Dia da semana em que o funcionario vai trabalhar 
         HorarioCome = int do horario que o funcinario vai começar a trabalhar
        HorarioFunal = Horario Final que o funcionario vai trabalhar
        */
        String hora = "";
        String cont1;
        for(int cont = Integer.valueOf(HorarioCome) ; cont != Integer.valueOf(HorarioFinal) + 1;cont ++){
         // Com isso temos uma tupla para cada horario 
         cont1 = ""+ cont;                  
         ins.InserirCadastroFuncionario(dia, cont1,mes,ano,funcionario);
        }
        
        
    }  
    public void InserirCadastroInicial(String Email,String Senha,int Tipo,String usuario){
        /*Email = String Email
        Senha = String senha
        Nome = String nome
        telefone = Integer/tinyInt telefone
        Cpf = Caracter(11) CPF
        Tipo = integer de 0 a 2
        */
        ins.InserirCadastroIncial(Email, Senha, Tipo, usuario);
    }
     public void InserirDeclaraçãoFuncionario(int Declaração){
         // Declaração é uma variavel que diz se o cliente compareceu a consulta que devia ( 0 = não , 1 = sim)
         ins.InserirDeclaraçãoPresença(Declaração);
     }
     public void InserirDocumentaçãoCliente(String nome,String Cpf,String Telefone,String email,String endereco,String usuario){
         /* nome = String nome
            Cpf = Integer Cpf
            Telefone = String Telefone
            Email = String Emails          
         */
         ins.InserirDocumentaçãoCliente(nome, Cpf, Telefone, email, endereco, usuario);
     }
     public void InserirServiçoAdmin(String serviço,int cod,String preço){
         /*
         String serviço = nome do serviço
         String preço = preço do servico
         Cod = int Codigo do serviço
         */
         ins.InserirServiçosAdmin(serviço, preço, cod + 1);
     }
     public void InserirServiçosFuncionario(String login,int cod){
         /*
         Adicionar em uma table o login e o serviço do usuario
         */
         ins.InserirServiçoFuncionario(cod, login);
     }
     
     
}

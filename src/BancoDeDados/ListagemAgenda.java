/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;
import javax.swing.JList;

/**
 *
 * @author Maria do Carmo
 */
public class ListagemAgenda {
    
    public void ListarAgendamentoCliente(JList lista){
          /*
              Mostra ao Cliente quais horarios estão disponiveis para marcar uma consulta     
          */ 

        try { 
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.agenda where numero is null";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             
             while(rs.next()){
                 dlm.addElement("Hora da consulta :"+ rs.getString("hora")+", Dia : "+ rs.getString("dia")+
                         ", Mes:"+ rs.getString("mes")+ ", Ano:"+ rs.getString("ano")+ ", Atendente :"+ rs.getString("login"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      
     public void ListarVisualizar_Adminstrador_Funcionario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.agenda";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 if(rs.getString("numero") != null){
                    dlm.addElement("Hora :"+rs.getString("hora") + ": Dia :"+rs.getString("dia")+": Mes :"+rs.getString("mes")+" Com um clinte marcado nesse horario");
                 }else{ 
                    dlm.addElement("Hora :"+rs.getString("hora") + ": Dia :"+rs.getString("dia")+": Mes :"+rs.getString("mes"));
             
             }
            }
             
           
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     
      public void ListarAgendaMudanca(JList lista,String dia,String hora,String mes,String serv){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.agenda where dia = ('"+dia+"') and hora = ('"+hora+"') and mes = ('"+mes+"') and login = ('"+serv+"');";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Hora da consulta :"+ rs.getString("hora")+", Dia : "+ rs.getString("dia")+
                         ", Mes:"+ rs.getString("mes")+ ", Ano:"+ rs.getString("ano")+ ", Atendente :"+ rs.getString("login"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListarNomeFuncionario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.funcionario";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
            dlm.addElement(rs.getString("login"));
      
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      
      
}

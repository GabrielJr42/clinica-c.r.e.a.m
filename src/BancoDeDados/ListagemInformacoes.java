/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;

/**
 *
 * @author Maria do Carmo
 */
public class ListagemInformacoes {
    
     public void ListarServiços(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.servicos";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("nome")+" - " + rs.getString("cod"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public String ListarNomeServico(int cod){
         try {
            
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.Servicos where cod ='"+cod+"' ";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                
                    return rs.getString("nome");    
                
             }
            
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
     }
       public String ListarPreçoServico(int cod){
         try {
            
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.Servicos where cod ='"+cod+"' ";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                
                    return rs.getString("preco");    
                
             }
            
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
     }
      public void ListarInformaçõesGerais(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica2.Cliente";                   
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement("Nome registrado: "+ rs.getString("nome"));
                 dlm.addElement("CPF registrado: "+ rs.getString("cpf"));
                dlm.addElement("Email registrado: "+ rs.getString("email"));
               dlm.addElement("telefone registrado: "+ rs.getInt("telefone"));
              dlm.addElement("Voçê não compareceu: "+ rs.getInt("aviso") +" vez(es)( Ao não comparecer 3 vezes , tomaram penalidades" ); 
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListarSalario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select salario from clinica2.TabelFunc";                     
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("salario"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
      public void ListarSalarioFuncionario(JList lista){
         try {
             lista.removeAll();
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select caixa from clinica2.funcionario";                     
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                 dlm.addElement(rs.getString("caixa"));
             }
             lista.setModel(dlm);
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public int LoginInicial(String email,String senha){
           int a = 0;
            try {
             if(email.equalsIgnoreCase("admin") && senha.equalsIgnoreCase("admin")){
                 a = 3;
             }
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.cliente";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                if(email.equalsIgnoreCase(rs.getString("login") )&& senha.equalsIgnoreCase(rs.getString("senha"))){
                    a =  1;
                }
             }
             c.close();
             
             Connection c1 = conexao.ObterConexao();
             String SQL1 = "Select * from clinica4.funcionario";
             Statement s1  = c1.createStatement();
             ResultSet rs1 = s1.executeQuery(SQL1);
             while(rs1.next()){
                if(email.equalsIgnoreCase(rs1.getString("login") )&& senha.equalsIgnoreCase(rs1.getString("senha"))){
                    a =  2;
                }
             }
             c1.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
             
         }
         return a;
     } 
     public int listarCodigoServiço(){
       // Com esse codigo é retirado do banco o ultimo "codigo" da table Serviços
         try {
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.Servicos where cod = (select max(cod) from clinica4.Servicos);";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
             return Integer.valueOf(rs.getString("cod"));
             }
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
     }
      public int listarCodigoServiçoFuncionario(String nome){
         try {
            
             Connection c = conexao.ObterConexao();
             String SQL = "select * from clinica4.Servicos where nome = ('"+nome+"');";
            Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             
             while(rs.next()){
              
             return Integer.valueOf(rs.getString("cod"));
             }
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return 0;
     }
       public void BoxServicos(JComboBox combo){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            m.addElement("SERVIÇOS FILTRAGEM");
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Serv_func");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
                //Retorna o nome do serviço apos adicionar o codigo no proprio
               String preco = preco = ListarPreçoServico(Integer.valueOf(rs.getInt("cod")));
               String name =  ListagemCodigos(rs.getInt("cod"));
               String a = name +" - "+preco+" - "+  rs.getString("login");
               m.addElement(a);                
            }   
            combo.setModel(m);
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
        public String ListagemCodigos(int cod){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Servicos where cod = ('"+cod+"')");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
               return  rs.getString("nome");   
            }   
           
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }
         public String listar1Codigos(int cod){
         try {
            
             DefaultListModel dlm = new DefaultListModel();
             Connection c = conexao.ObterConexao();
             String SQL = "Select * from clinica4.Servicos where cod ='"+cod+"' ";
             Statement s  = c.createStatement();
             ResultSet rs = s.executeQuery(SQL);
             while(rs.next()){
                
                    return rs.getString("nome");    
                
             }
            
             c.close();
         } catch (SQLException ex) {
             Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
         }
         return null;
     }
        public void BoxServicosFuncionarios(JComboBox combo,String nome){
        try {
            DefaultComboBoxModel m = new DefaultComboBoxModel();
            Connection c = conexao.ObterConexao();
            PreparedStatement p =  c.prepareStatement("Select * from clinica4.Serv_func where login = ('"+nome+"')");
            ResultSet rs = p.executeQuery();
            while(rs.next()){
               // Data_Serie a = new Data_Serie(rs.getString("nome_serie"),rs.getInt("data_de_estreia"));
                int cod = rs.getInt("cod");
               //System.out.println("a");
                String preco = ListarPreçoServico(cod);
                String a = listar1Codigos(cod);
                m.addElement(a +" - "+preco);      
            }   
            combo.setModel(m);
        } catch (SQLException ex) {
            Logger.getLogger(Listagem.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

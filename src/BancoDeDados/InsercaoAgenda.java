/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BancoDeDados;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Maria do Carmo
 */
public class InsercaoAgenda {
    
    public void UpdateAgendamentoCliente(String day,String hora,String mes,String ano,String Funcionario,String numero){
          /* horario = horario do Cliente
         day = integer de 1 a 7 com os dias da semana     
         mes = integer 1  a 12
         ano = integer de 2019 a 2999
         Funcionario = String Funcionario
         Codigo = String Codigo
        */       
            
        try {        
            Connection c = conexao.ObterConexao();
            String SQL = " update clinica4.Agenda" +
                           " set numero = ?" +
                           " where dia = ? and hora = ? and ano = ? and mes = ? and login = ?";
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(1, numero);
            s.setString(2, (day));
            s.setString(3, (hora));
            s.setString(4, (ano));
            s.setString(5, (mes));
            s.setString(6, Funcionario);           
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirCadastroFuncionario(String day,String hora,String mes,String ano,String Funcionario){
          /*    DiaSem = Dia da semana em que o funcionario vai trabalhar 
                HorarioCome = int do horario que o funcinario vai começar a trabalhar
                HorarioFunal = Horario Final que o funcionario vai trabalhar
          */
         try {    
            Connection c = conexao.ObterConexao();
            System.out.println("Inserir Cadastro Funcionario :"+mes);
            String SQL = "Insert into clinica4.agenda(dia,hora,ano,mes,login) VALUES (?,?,?,'"+mes+"',?) ";
            PreparedStatement s = c.prepareStatement(SQL);       
            s.setString(1, (day));
            s.setString(2, (hora));
            s.setString(3, (ano));     
            s.setString(4, Funcionario);           
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void InserirSalario(int salario,String login){
        try {
            Connection c = conexao.ObterConexao();
            String SQL = "Update clinica4.funcionario set salario = ? where login = ?" ;
            PreparedStatement s = c.prepareStatement(SQL);
            s.setString(2,login);
            s.setInt(1, salario);                    
            s.executeUpdate();
            c.close();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            Logger.getLogger(Insercao.class.getName()).log(Level.SEVERE, null, ex);
        }
}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Informacoes;

import BancoDeDados.InsercaoInformacoes;

/**
 *
 * @author Maria do Carmo
 */
public class Cadastro_Inicial_codigo {
    private String Email,senha,login,nome,cpf,Endereco,numero;
    private int telefone,tipo;
    // tipo = se for Cliente ou funcionario (1 = Cliente , 2 = Funcionario)
    InsercaoInformacoes ins = new InsercaoInformacoes();
    public Cadastro_Inicial_codigo(String Email, String senha, String login, String nome, String cpf, String Endereco, int telefone, int tipo,String numero) {
        this.Email = Email;
        this.senha = senha;
        this.login = login;
        this.nome = nome;
        this.cpf = cpf;
        this.Endereco = Endereco;
        this.telefone = telefone;
        this.tipo = tipo;
        this.numero = numero;
    }
    public void Inserir(){
        if(tipo == 1){
            ins.InserirDocumentaçãoCliente(nome, cpf, telefone, Email, Endereco, login, senha, numero);
        }else if(tipo == 2){
            ins.InserirDocumentaçãoFuncionario(nome, cpf, telefone, Email, Endereco, login, senha);
        }
        
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Agendamento;

import BancoDeDados.InsercaoInformacoes;
import BancoDeDados.ListagemInformacoes;
import javax.swing.JList;

/**
 *
 * @author Maria do Carmo
 */
public class SeleçãoServiços_Funcionario_Codigo {
    private String login,numero;
    
    public SeleçãoServiços_Funcionario_Codigo(String numero, String login) {
        this.login = login;
        this.numero = numero;
    }
    
    ListagemInformacoes lis = new ListagemInformacoes();
    InsercaoInformacoes ins = new InsercaoInformacoes();
    public void Visualizar(JList lista){
        lis.ListarServiços(lista);
    }
    public void Inserir(){
       // System.out.println(numero);
       //preco = lis.ListarPreçoServico(Integer.valueOf(numero));
       ins.InserirServiçoFuncionario(Integer.valueOf(numero), login);
    }
    public String UsandoOmouse(JList jList1){
         String J = (String) jList1.getModel().getElementAt(jList1.getSelectedIndex()); 
         int j = Integer.valueOf(J.substring(J.length() - 1, J.length()));
         return (j + "");
    }
}

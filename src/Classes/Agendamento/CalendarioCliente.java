/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Agendamento;

import BancoDeDados.Listagem;
import com.toedter.calendar.JCalendar;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JPanel;

/**
 *
 * @author Maria do Carmo
 */

public class CalendarioCliente {
    Listagem lis = new Listagem();
    public JCalendar DiasMarcados(JCalendar calendar){
        int mon = calendar.getMonthChooser().getMonth() + 1;
        //System.out.println("mes "+ mon);
        int yr = calendar.getYearChooser().getYear();
        //System.out.println("ano "+ yr); 
        JCalendar calendar1 = lis.CalendarCliente(calendar);
        JPanel jPanel = calendar.getDayChooser().getDayPanel();
        Component component[] = jPanel.getComponents();
       // component[8].setBackground(Color.green);
        return calendar1;
    }
}

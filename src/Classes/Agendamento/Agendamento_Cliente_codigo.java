/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Agendamento;

import BancoDeDados.InsercaoAgenda;
import BancoDeDados.ListagemAgenda;
import BancoDeDados.ListagemInformacoes;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 *
 * @author Maria do Carmo
 */
public class Agendamento_Cliente_codigo {
    private String dia,mes,hora,servico;
    ListagemAgenda lis = new ListagemAgenda();
    InsercaoAgenda ins = new InsercaoAgenda();
    ListagemInformacoes lisInf = new ListagemInformacoes();
    public Agendamento_Cliente_codigo(String dia, String mes, String hora, String servico) {
        this.dia = dia;
        this.mes = mes;
        this.hora = hora;
        this.servico = servico;
    }
    
    public void MarcandoHorario(JList Horarios){
         String J = (String) Horarios.getModel().getElementAt(Horarios.getSelectedIndex()); 
           System.out.println(J);
          //Seperando o dia,mes,ano,Serviço na String
           String hora1 = J.substring(18, 20);
           System.out.println("Hora :"+hora1);
           String dia1 = J.substring(28, 30);
           System.out.println("Dia :"+dia1);
           String mes1 = J.substring(36, 38);
           System.out.println("Mes :"+mes1);
           String ano = J.substring(44, 48);
           System.out.println("Ano :"+ano);
           String serv = J.substring(61, J.length());    
           System.out.println("Servico :"+serv);
           String numero = JOptionPane.showInputDialog(null, "Qual seu numero Usuario ?");
           ins.UpdateAgendamentoCliente(dia1, hora1, mes1, ano, serv, numero);
           JOptionPane.showMessageDialog(null, "Horario Marcado!");
    }
    public void Filtragem(JList Horarios){
         lis.ListarAgendaMudanca(Horarios,dia,hora,mes,servico);
    }
    public void Visualizar(JList Horarios,JComboBox Servi){
        lis.ListarAgendamentoCliente(Horarios);
        lisInf.BoxServicos(Servi);
    }
}

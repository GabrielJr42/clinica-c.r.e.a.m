/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes.Agendamento;

import BancoDeDados.BancoDeDados;
import BancoDeDados.InsercaoAgenda;
import BancoDeDados.ListagemInformacoes;
import javax.swing.JComboBox;
import javax.swing.JList;

/**
 *
 * @author Maria do Carmo
 */
public class Cadastro_Funcionario_codigo {
   private int Ano,HorasCom,MinutosCom,HorasTerm;
    private String servico,hora,Dia,Mes,HorarioComeca,HorarioTermina;
    InsercaoAgenda ins = new InsercaoAgenda();
    ListagemInformacoes lis = new ListagemInformacoes();
    public Cadastro_Funcionario_codigo(String HorarioComeca,String HorarioTermina,String Dia,String Mes,int Ano,String servico) {
        this.Dia = Dia;
        this.Mes = Mes;
        this.Ano = Ano;
        this.HorarioComeca = HorarioComeca;
        this.HorarioTermina = HorarioTermina;
        this.servico = servico;    
    }
   public void VisualizarBox(JComboBox box,String login){
       lis.BoxServicosFuncionarios(box, login);
   }

    public Cadastro_Funcionario_codigo() {
    }
    public void InserirAgendaFuncionario(){  
         HorasCom = Integer.parseInt(HorarioComeca.substring(0, 2));
         MinutosCom = Integer.parseInt(HorarioComeca.substring(3, 5));
         HorasTerm = Integer.parseInt(HorarioTermina.substring(0, 2));
         MinutosCom = Integer.parseInt(HorarioComeca.substring(3, 5));
         for(;HorasCom != HorasTerm ;MinutosCom +=+30){
         if(MinutosCom == 60){
             HorasCom += + 1;
             MinutosCom = 0;
         }
         hora = HorasCom + ":"+MinutosCom;  
         //System.out.println(hora);
         ins.InserirCadastroFuncionario(Dia,hora,Mes,Integer.toString(Ano),servico);
        }
    }
}
